# Copyright (c) 2014,
# Mathias Kuhring, KuhringM@rki.de, Robert Koch Institute, Germany, 
# All rights reserved. For details, please note the license.txt.

# calculation of N50
N50 <- function(contigLengths){
  median(rep(contigLengths,contigLengths))
}


baseconf <- function(corecoverage, coverage, prob){
  output <- vector(mode="numeric", length=length(coverage))
  for (i in 1:length(coverage)){
    output[i] <- binom.test(corecoverage[i], coverage[i], prob, "greater")$p.value
  }
  return(output)
}

mybinom <- function(covpair){
  if (covpair[2]<covpair[1]) covpair[2] <- covpair[1]
  return(binom.test(covpair[1], covpair[2], 0.98, "greater")$p.value)
}

multibinom <- function(covpairs){
  return(apply(cbind(unlist(covpairs[1]),unlist(covpairs[2])), 1, mybinom))
}

# insert vector "input" with colname "name" 
# into data.frame "data" after column "after"
insert <- function(data, input, name, after){
  pos <- which(names(data)==after)
  output <- cbind(data[1:pos], input, data[(pos+1):length(data)])
  colnames(output)[(pos+1)] <- name
  return(output)
}