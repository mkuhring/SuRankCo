# Copyright (c) 2014,
# Mathias Kuhring, KuhringM@rki.de, Robert Koch Institute, Germany, 
# All rights reserved. For details, please note the license.txt.

# functions handling the training and prediction of contig qualities with random forest
#
# author:   Mathias Kuhring


# random forest training of contig qualities
# returns a list of randomForest objects, one per target column
rfTraining <- function(input, targets, balance=FALSE){
  if (DEBUGGING){ print('forest training') }
  
  rfList <- vector("list", ncol(targets))
  names(rfList) <- names(targets)
  library("randomForest")
  for (i in 1:ncol(targets)){
    if (DEBUGGING){ print(i) }
    
    # balancing
    index <- 1:dim(input)[1]
    if (balance){
      if (DEBUGGING){
		print("balancing...")
		print(paste("size before:", length(index)))
	  }
      index <- balancingUp(training[,i])
      if (DEBUGGING){ print(paste("size after:", length(index))) }
    }

#     vars <- 3:ncol(input)
    
    # training
    rfList[[i]] <- randomForest(x=input[index, ], y=targets[index,i], 
                                importance=TRUE, localImp=TRUE, ntree=100)
  }
  
  if (DEBUGGING){ print('training done') }
  return(rfList)
}


# random forest prediction of contig qualities
# returns a list of predictions per metric
scorePrediction <- function(rfs, input, type){
  if (DEBUGGING){ print("score prediction") }
  
  predictions <- vector("list", length(rfs))
  names(predictions) <- names(rfs)
  
#   vars <- 3:ncol(input)

  require("randomForest") 
  for (i in 1:length(rfs)){
    # prediction
    predictions[[i]] <- predict(rfs[[i]], input, type=type)
  }
  
  if (DEBUGGING){ print("prediction done") }
  return(predictions)
}


selectContigs <- function(features, scores){
  
  number <- length(features)
  combination <- vector(mode="list", number)
  
  for (i in 1:number){
    features[[i]]$Assembly <- NULL
    features[[i]]$ReadQuality <- NULL
    scores[[i]]$Assembly <- NULL
    scores[[i]]$Reference <- NULL
    combination[[i]] <- merge(features[[i]], scores[[i]], by="ContigID")
  }
  
  return( do.call(rbind, combination) )
}

dataFilter <- function(data){
  if (DEBUGGING){ print('data filtering') }
  check <- vector('logical',nrow(data))
  for (i in 1:ncol(data)){
    check <- check | is.infinite(data[,i])
    check <- check | is.nan(data[,i])
    check <- check | is.na(data[,i])
  }
  
  # (Temporary) Remove Contigs with MatchCount bigger than Length (shouldn't be)
  if ("NormedMatchCount1" %in% names(data)){
    check <- check | data$NormedMatchCount1 > 1
  }
  
  #   # Remove MIRA Contigs with repeats
  #   check <- check | grepl("*rep*", data$ContigID)
  
  data <- data[!check,]
  if (sum(check)>0) cat(paste("warning: removed", sum(check), " inconsistent contigs\n"))
  
  if (DEBUGGING){ print('filtering done') }
  return(data)
}