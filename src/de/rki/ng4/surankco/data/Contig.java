package de.rki.ng4.surankco.data;

import java.util.Vector;

/*
 * Addapter Class for different Contig data, e.g. from Ace, Fasta, ...
 */
public class Contig {
	
	private String id;
	private int length;
	private int baseCount;
	private int readCount;
	private int baseSeqmentCount;

	private String consensusSequence;
	private Vector<Integer> qualityValues;
	private int[][] nucleotideCounts;
	
	private Vector<Read> reads;

		
	public Contig(String id) {
		this.id = id;
	}
	
	// getter
	public String getId() {
		return id;
	}
	
	public Integer getLength() {
		return length;
	}

	public Integer getBaseCount() {
		return baseCount;
	}

	public Integer getReadCount() {
		return readCount;
	}

	public Integer getBaseSeqmentCount() {
		return baseSeqmentCount;
	}

	public Vector<Read> getReads() {
		return reads;
	}

	public String getConsensusSequence() {
		return consensusSequence;
	}
	
	public Vector<Integer> getQualityValues() {
		return qualityValues;
	}

	public int[][] getNucleotideCount() {
		return nucleotideCounts;
	}

	// setter
	public void setId(String id) {
		this.id = id;
	}

	public void setLength(int length) {
		this.length = length;
	}
	
	public void setBaseCount(int baseCount) {
		this.baseCount = baseCount;
	}

	public void setReadCount(int readCount) {
		this.readCount = readCount;
	}

	public void setBaseSeqmentCount(int baseSeqmentCount) {
		this.baseSeqmentCount = baseSeqmentCount;
	}

	public void setConsensusSequence(String consensusSequence) {
		this.consensusSequence = consensusSequence;
	}

	public void setQualityValues(Vector<Integer> qualityValues) {
		this.qualityValues = qualityValues;
	}

	public void setNucleotideCount(int[][] nucleotideCount) {
		this.nucleotideCounts = nucleotideCount;
	}
	
	public void setReads(Vector<Read> reads) {
		this.reads = reads;
	}
}
