/* Copyright (c) 2014,
 * Mathias Kuhring, KuhringM@rki.de, Robert Koch Institute, Germany, 
 * All rights reserved. For details, please note the license.txt.
 */

package de.rki.ng4.surankco.data;

import java.util.HashMap;
import java.util.Vector;

import de.rki.ng4.surankco.feature.Main;
import de.rki.ng4.surankco.files.Fastq;
import de.rki.ng4.surankco.files.Qual;

public class Reads {

	public Vector<String> readId;
	public Vector<Vector<Integer>> readQuality;
	
	HashMap<String,Integer> lengthMap;
	HashMap<String,Vector<Integer>> qualityMap;
	
	public Reads(String filename, String parameter) {
		readId = new Vector<String>();	
		readQuality = new Vector<Vector<Integer>>();
		
		String readfile_suffix = filename.substring(filename.lastIndexOf(".")).toLowerCase();
		
		if (readfile_suffix.matches(".qual") || readfile_suffix.matches(".qua") ){
			new Qual(filename, this);
		}
		else if(readfile_suffix.matches(".fastq")){
			new Fastq(filename, this, parameter);
		}
		
		if (Main.DEBUGMODE){
			int readnumber = Math.min(readId.size(), 5);
			System.out.println("read quality control view (" + readnumber + " of " + readId.size() + " reads)");
			for (int i=0; i<readnumber; i++){
				System.out.println(readId.get(i));
				System.out.println(readQuality.get(i));
			}
		}
		
		calcQualityMap();
	}

	
	private void calcQualityMap(){
		qualityMap = new HashMap<String, Vector<Integer>>();
		for (int i=0; i<readId.size(); i++){
			qualityMap.put(readId.get(i), readQuality.get(i));
		}
	}
	
	public Vector<Vector<Integer>> getPooledReadQualities(Vector<Vector<String>> contigReads){
		
		Vector<Vector<Integer>> qualities = new Vector<Vector<Integer>>();
		
		for (Vector<String> vs : contigReads){
			qualities.add(new Vector<Integer>());
			for (String s : vs){
				try {
					qualities.lastElement().addAll(qualityMap.get(s));
				}
				catch (NullPointerException e){
//					e.printStackTrace();
					System.out.println("\tread missing (check split.regex): " + s);
				}
			}
		}
		return qualities;
	}
	
	public Vector<Vector<Integer>> getReadLengths(Vector<Vector<String>> contigReads){
		Vector<Vector<Integer>> lengths = new Vector<Vector<Integer>>();
		for (Vector<String> vs : contigReads){
			lengths.add(new Vector<Integer>());
			for (String s : vs){
				try {
					lengths.lastElement().add(qualityMap.get(s).size());
				}
				catch (NullPointerException e){
//					e.printStackTrace();
					System.out.println("\tread missing (check split.regex): " + s);
				}
			}
		}
		return lengths;
	}
	
	public void readMatchCheck(Vector<Vector<Integer>> readMapping, Vector<String> contigId, String contigfile, String readfile){
		Vector<String> contigsWithNoMatches = new Vector<String>();
		
		for (int i=0; i<readMapping.size(); i++){
			if (readMapping.get(i).size()==0){
				contigsWithNoMatches.add(contigId.get(i));
			}
		}
		
		if (contigsWithNoMatches.size() > 0){
			StringBuffer output = new StringBuffer();
			output.append("error: no reads matching in " + contigfile + " and " + readfile + " for contigs:\n");
			output.append(contigsWithNoMatches.toString());
			System.out.println(output.toString());
			System.exit(1);
		}
	}
}
