/* Copyright (c) 2014,
 * Mathias Kuhring, KuhringM@rki.de, Robert Koch Institute, Germany, 
 * All rights reserved. For details, please note the license.txt.
 */

package de.rki.ng4.surankco.data;

public class AceRead {

	private int lineAF;
	private int lineRD;
	private int lineQA;
		
	// AF
	private String id;
	private char orientation;
	private int offset;
	
	// RD
	private int paddedLength;
	
	private String paddedSequence;
	
	// QA
	private int alignClippingStart;
	private int alignClippingEnd;

	public AceRead(){
	}
	
	public void addAF(String line){
		String[] splits = line.split(" ");
		id = splits[1];
		orientation = splits[2].charAt(0);
		offset = Integer.parseInt(splits[3]);
	}
	
	public void addRD(String line){
		// RD TBEOG48.y1 619 0 0
		String[] splits = line.split(" ");
		id = splits[1];
		paddedLength = Integer.parseInt(splits[2]);
	}

	public void addQA(String line){
		String[] splits = line.split(" ");
		alignClippingStart = Integer.parseInt(splits[3]);
		alignClippingEnd = Integer.parseInt(splits[4]);
	}
	
	public int getClippingLength(){
		return (alignClippingEnd-alignClippingStart+1);
	}

	/**
	 * @return the lineRD
	 */
	public int getLineRD() {
		return lineRD;
	}

	/**
	 * @param lineRD the lineRD to set
	 */
	public void setLineRD(int lineRD) {
		this.lineRD = lineRD;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the orientation
	 */
	public char getOrientation() {
		return orientation;
	}

	/**
	 * @param orientation the orientation to set
	 */
	public void setOrientation(char orientation) {
		this.orientation = orientation;
	}

	/**
	 * @return the paddedSequence
	 */
	public String getPaddedSequence() {
		return paddedSequence;
	}

	/**
	 * @param paddedSequence the paddedSequence to set
	 */
	public void setPaddedSequence(String paddedSequence) {
		this.paddedSequence = paddedSequence;
	}

	/**
	 * @return the offset
	 */
	public int getOffset() {
		return offset;
	}

	/**
	 * @param offset the offset to set
	 */
	public void setOffset(int offset) {
		this.offset = offset;
	}

	/**
	 * @return the paddedLength
	 */
	public int getPaddedLength() {
		return paddedLength;
	}

	/**
	 * @param paddedLength the paddedLength to set
	 */
	public void setPaddedLength(int paddedLength) {
		this.paddedLength = paddedLength;
	}

	/**
	 * @return the alignClippingStart
	 */
	public int getAlignClippingStart() {
		return alignClippingStart;
	}

	/**
	 * @param alignClippingStart the alignClippingStart to set
	 */
	public void setAlignClippingStart(int alignClippingStart) {
		this.alignClippingStart = alignClippingStart;
	}

	/**
	 * @return the alignClippingEnd
	 */
	public int getAlignClippingEnd() {
		return alignClippingEnd;
	}

	/**
	 * @param alignClippingEnd the alignClippingEnd to set
	 */
	public void setAlignClippingEnd(int alignClippingEnd) {
		this.alignClippingEnd = alignClippingEnd;
	}

	public int getLineAF() {
		return lineAF;
	}

	public void setLineAF(int lineAF) {
		this.lineAF = lineAF;
	}

	public int getLineQA() {
		return lineQA;
	}

	public void setLineQA(int lineQA) {
		this.lineQA = lineQA;
	}
}