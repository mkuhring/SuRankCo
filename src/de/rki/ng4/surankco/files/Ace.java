/* Copyright (c) 2014,
 * Mathias Kuhring, KuhringM@rki.de, Robert Koch Institute, Germany, 
 * All rights reserved. For details, please note the license.txt.
 */

package de.rki.ng4.surankco.files;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.Vector;

import de.rki.ng4.surankco.data.AceRead;
import de.rki.ng4.surankco.data.AceContig;


public class Ace {

	private String fileName;
	private Vector<AceContig> contigs;

	public Ace(String fileName){
		this.fileName = fileName;
		contigs = new Vector<AceContig>();
		try {
			LineNumberReader acebr = new LineNumberReader(new FileReader(new File(fileName)));
			try {
				String line;
				int readCounter = 0;
				while ((line = acebr.readLine()) != null){
					if (line.startsWith("CO ")){
						contigs.add(new AceContig(line));
						contigs.lastElement().setLineCO( acebr.getLineNumber()-1 );
						readCounter = 0;
					}
					else if (line.startsWith("BQ")){
						contigs.lastElement().setLineBQ( acebr.getLineNumber()-1 );
					}
					else if (line.startsWith("AF ")){
						contigs.lastElement().addRead(new AceRead());
						contigs.lastElement().getReads().lastElement().addAF(line);
					}
					else if (line.startsWith("RD ")){
						readCounter++;
						
						contigs.lastElement().getReads().get(readCounter-1).addRD(line);
						contigs.lastElement().getReads().get(readCounter-1).setLineRD( acebr.getLineNumber()-1 );
						
						StringBuffer readSeq = new StringBuffer();
						while ((line = acebr.readLine()) != null && line.length() != 0){
							readSeq.append(line);
						}
						contigs.lastElement().getReads().get(readCounter-1).setPaddedSequence( readSeq.toString() );
					}
					else if (line.startsWith("QA ")){
						contigs.lastElement().getReads().get(readCounter-1).addQA(line);
					}
				}
			}
			finally {
				acebr.close();
			}
		} 
		catch (FileNotFoundException e) {
			e.printStackTrace();
			System.out.println("error: problems reading file: " + fileName);
			System.exit(1);
		} 
		catch (IOException e) {
			e.printStackTrace();
			System.out.println("error: problems reading file: " + fileName);
			System.exit(1);
		}
	}

	public void readConsensusSequences(){
		String line;
		StringBuffer consensus;
		try {
			BufferedReader acebr = new BufferedReader(new FileReader(new File(fileName)));
			int lineCounter = 0;
			try {
				for (AceContig c : contigs){
					consensus = new StringBuffer();
					while (lineCounter < c.getLineCO()){
						if ((line = acebr.readLine()) == null) break;
						lineCounter++;
					}

					if ((line = acebr.readLine()) != null && line.startsWith("CO")){
						lineCounter++;
						while ((line = acebr.readLine()) != null && line.length() != 0){
							lineCounter++;
							consensus.append(line);
						}
					}
					lineCounter++;
					c.setConsensusSequence( consensus.toString().toUpperCase() );
				}
			}
			finally {
				acebr.close();
			}
		} 
		catch (FileNotFoundException e) {
			e.printStackTrace();
			System.out.println("error: problems reading file: " + fileName);
			System.exit(1);
		} 
		catch (IOException e) {
			e.printStackTrace();
			System.out.println("error: problems reading file: " + fileName);
			System.exit(1);
		}
	}

	public void readQualityValues(){
		String line;
		String[] splits;
		try {
			BufferedReader acebr = new BufferedReader(new FileReader(new File(fileName)));
			int lineCounter = 0;
			try {
				for (AceContig c : contigs){
					while (lineCounter < c.getLineBQ()){
						if ((line = acebr.readLine()) == null) break;
						lineCounter++;
					}

					if ((line = acebr.readLine()) != null && line.matches("BQ")){
						lineCounter++;
						while ((line = acebr.readLine()) != null && line.length() != 0){
							lineCounter++;
							splits = line.split(" ");
							for (String s : splits){
								c.getQualityValues().add(Integer.parseInt(s));
							}
						}
					}
					lineCounter++;
				}
			}
			finally {
				acebr.close();
			}
		} 
		catch (FileNotFoundException e) {
			e.printStackTrace();
			System.out.println("error: problems reading file: " + fileName);
			System.exit(1);
		} 
		catch (IOException e) {
			e.printStackTrace();
			System.out.println("error: problems reading file: " + fileName);
			System.exit(1);
		}
	}

	/**
	 * @return the contigs
	 */
	public Vector<AceContig> getContigs() {
		return contigs;
	}

	/**
	 * @param contigs the contigs to set
	 */
	public void setContigs(Vector<AceContig> contigs) {
		this.contigs = contigs;
	}
}
