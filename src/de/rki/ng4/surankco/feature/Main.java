/* Copyright (c) 2014,
 * Mathias Kuhring, KuhringM@rki.de, Robert Koch Institute, Germany, 
 * All rights reserved. For details, please note the license.txt.
 */

package de.rki.ng4.surankco.feature;

import java.util.Arrays;
import java.util.Vector;

import de.rki.ng4.surankco.data.Assembly;
import de.rki.ng4.surankco.data.AssemblyAce;
import de.rki.ng4.surankco.data.AssemblySam;
import de.rki.ng4.surankco.files.ExportR;
import de.rki.ng4.surankco.tools.Statistic;

/**
 * Part of the feature generation/calculation of SuRankCo
 * Main procedure, initializes file imports, features extractions and file exports
 * 
 * @author Mathias Kuhring
 *
 */
public class Main {

	// debugging
	public final static boolean DEBUGMODE = false;
	
	// simple timer use with timerStart() and timerStop()
	public static long timer;
	
	
	/**
	 * 
	 * 
	 * @param args Arguments and their order are described in the surankco-feature R-script 
	 */
	public static void main(String[] args) {
		
		if (Main.DEBUGMODE){ System.out.println(Arrays.toString(args)); }
		
		// prepare export object to collect features
		ExportR e = new ExportR();

		// import ace file and read qualities
		if (Main.DEBUGMODE){ System.out.println("-> import assembly"); }
		String contigfile = args[0];
		String readfile = args[1];
		String fastqParameter = args[5];
		
		boolean ace = contigfile.endsWith("ace");
		Assembly assembly;
		if (ace){
			assembly = new AssemblyAce(contigfile, readfile, fastqParameter);			
		}
		else{
			assembly = new AssemblySam(contigfile, readfile);
		}
		System.gc();
		
		// contig lenght filter
		assembly.lengthFilter(Integer.parseInt(args[8]));
		
		// calculate several features
		Several features = new Several(assembly.getContigs());

		e.addStringVariable("ContigID", features.getIds());
		e.addIntegerVariable("Length", features.getContigLength());
		if (ace){ // !!! no BaseCount and BaseSegmentCount for sam contigs yet, because no padded sequence available yet
			e.addIntegerVariable("BaseCount", features.getBaseCounts());
			e.addIntegerVariable("BaseSegmentCount", features.getBaseSeqmentCounts());
		}
		e.addIntegerVariable("ReadCount", features.getReadCounts());
		e.addDoubleVariable("ReadComplementFraction", features.getReadComplementFraction());

		if (Main.DEBUGMODE){ System.out.println("-> contig qualities"); }
		
		if (ace){ // !!! no quality values for sam contigs yet
			Vector<Vector<Integer>> contigQualities = features.getQualityValues();
			e.addDoubleVariable("ContigQualitiesMean", Statistic.means(contigQualities));
			e.addDoubleVariable("ContigQualitiesSD", Statistic.sds(contigQualities));
			e.addDoubleVariable("ContigQualitiesMedian", Statistic.medians(contigQualities));
			e.addDoubleVariable("ContigQualitiesMAD", Statistic.mads(contigQualities));
			e.addDoubleVariable("ContigQualitiesMin", Statistic.mins(contigQualities));
			e.addDoubleVariable("ContigQualitiesMax", Statistic.maxs(contigQualities));
		}

		System.gc();
		
		// calculate read quality features
		if (Main.DEBUGMODE){ System.out.println("-> read qualities"); }
		
		String regex = args[6];
		
		Vector<Vector<String>> readIds = features.getReadIds(regex);
		Vector<Vector<Integer>> readQualities = assembly.getPooledReadQualities(readIds);
	
		e.addDoubleVariable("ReadQualitiesMean", Statistic.means(readQualities));
		e.addDoubleVariable("ReadQualitiesSD", Statistic.sds(readQualities));
		e.addDoubleVariable("ReadQualitiesMedian", Statistic.medians(readQualities));
		e.addDoubleVariable("ReadQualitiesMAD", Statistic.mads(readQualities));
		e.addDoubleVariable("ReadQualitiesMin", Statistic.mins(readQualities));
		e.addDoubleVariable("ReadQualitiesMax", Statistic.maxs(readQualities));

		if (Main.DEBUGMODE){ System.out.println("-> read lengths"); }
		
		Vector<Vector<Integer>> readLengths = assembly.getReadLengths(readIds);
		
		Vector<Double> readLengthsMean = Statistic.means(readLengths);
		e.addDoubleVariable("ReadLengthsMean", readLengthsMean);
		e.addDoubleVariable("ReadLengthsSD", Statistic.sds(readLengths));
		e.addDoubleVariable("ReadLengthsMedian", Statistic.medians(readLengths));
		e.addDoubleVariable("ReadLengthsMAD", Statistic.mads(readLengths));
		e.addDoubleVariable("ReadLengthsMin", Statistic.mins(readLengths));
		e.addDoubleVariable("ReadLengthsMax", Statistic.maxs(readLengths));
		
		assembly = null;
		System.gc();
		
		// calculate read length features
		if (Main.DEBUGMODE){ System.out.println("-> read lengths padded"); }	
		Vector<Vector<Integer>> readPaddedLengths = features.getReadPaddedLengths();
		e.addDoubleVariable("ReadLengthsPaddedMean", Statistic.means(readPaddedLengths));
		e.addDoubleVariable("ReadLengthsPaddedSD", Statistic.sds(readPaddedLengths));
		e.addDoubleVariable("ReadLengthsPaddedMedian", Statistic.medians(readPaddedLengths));
		e.addDoubleVariable("ReadLengthsPaddedMAD", Statistic.mads(readPaddedLengths));
		e.addDoubleVariable("ReadLengthsPaddedMin", Statistic.mins(readPaddedLengths));
		e.addDoubleVariable("ReadLengthsPaddedMax", Statistic.maxs(readPaddedLengths));

		if (Main.DEBUGMODE){ System.out.println("-> read lengths clipping"); }
		Vector<Vector<Integer>> readClippingLengths = features.getReadClippingLengths();
		e.addDoubleVariable("ReadLengthsClippingMean", Statistic.means(readClippingLengths));
		e.addDoubleVariable("ReadLengthsClippingSD", Statistic.sds(readClippingLengths));
		e.addDoubleVariable("ReadLengthsClippingMedian", Statistic.medians(readClippingLengths));
		e.addDoubleVariable("ReadLengthsClippingMAD", Statistic.mads(readClippingLengths));
		e.addDoubleVariable("ReadLengthsClippingMin", Statistic.mins(readClippingLengths));
		e.addDoubleVariable("ReadLengthsClippingMax", Statistic.maxs(readClippingLengths));

		if (Main.DEBUGMODE){ System.out.println("-> gc-content"); }
		e.addDoubleVariable("GcFraction", features.getGcContent());

		System.gc();
		
		// calculate coverage features
		if (Main.DEBUGMODE){ System.out.println("-> coverage"); timerStart(); }

		Vector<Vector<Integer>> coverageGlobal = features.getGlobalCoverages();
		e.addDoubleVariable("CoverageGlobalMean", Statistic.means(coverageGlobal));
		e.addDoubleVariable("CoverageGlobalSD", Statistic.sds(coverageGlobal));
		e.addDoubleVariable("CoverageGlobalMedian", Statistic.medians(coverageGlobal));
		e.addDoubleVariable("CoverageGlobalMAD", Statistic.mads(coverageGlobal));
		e.addDoubleVariable("CoverageGlobalMin", Statistic.mins(coverageGlobal));
		e.addDoubleVariable("CoverageGlobalMax", Statistic.maxs(coverageGlobal));
		if (Main.DEBUGMODE){ timerStop(); }
				
		if (Main.DEBUGMODE){ System.out.println("-> coverage 2"); timerStart(); }
		Coverage cov = new Coverage(coverageGlobal);
		if (cov.calcEndCovs(readLengthsMean)){
			e.addDoubleVariable("CoverageMinEndMean", cov.getCoverageMinEndMean());
			e.addDoubleVariable("CoverageMinEndSD", cov.getCoverageMinEndSD());
			e.addDoubleVariable("CoverageMinEndMedian", cov.getCoverageMinEndMedian());
			e.addDoubleVariable("CoverageMinEndMAD", cov.getCoverageMinEndMAD());
			e.addDoubleVariable("CoverageMinEndMin", cov.getCoverageMinEndMin());
			e.addDoubleVariable("CoverageMinEndMax", cov.getCoverageMinEndMax());
			e.addDoubleVariable("CoverageMaxEndMean", cov.getCoverageMaxEndMean());
			e.addDoubleVariable("CoverageMaxEndSD", cov.getCoverageMaxEndSD());
			e.addDoubleVariable("CoverageMaxEndMedian", cov.getCoverageMaxEndMedian());
			e.addDoubleVariable("CoverageMaxEndMAD", cov.getCoverageMaxEndMAD());
			e.addDoubleVariable("CoverageMaxEndMin", cov.getCoverageMaxEndMin());
			e.addDoubleVariable("CoverageMaxEndMax", cov.getCoverageMaxEndMax());
		}
		if (Main.DEBUGMODE){ timerStop(); }
	
		if (Main.DEBUGMODE){ System.out.println("-> core coverage"); timerStart(); }
		Vector<Vector<Integer>> coreCoverage = features.getCoreCoverages();
		e.addDoubleVariable("CoreCoverageGlobalMean", Statistic.means(coreCoverage));
		e.addDoubleVariable("CoreCoverageGlobalSD", Statistic.sds(coreCoverage));
		e.addDoubleVariable("CoreCoverageGlobalMedian", Statistic.medians(coreCoverage));
		e.addDoubleVariable("CoreCoverageGlobalMAD", Statistic.mads(coreCoverage));
		e.addDoubleVariable("CoreCoverageGlobalMin", Statistic.mins(coreCoverage));
		e.addDoubleVariable("CoreCoverageGlobalMax", Statistic.maxs(coreCoverage));
		if (Main.DEBUGMODE){ timerStop(); }
		
		// catch coverage vector size error
		boolean devError = false;
		for (int i=0; i<coverageGlobal.size(); i++){
			if (devError = coverageGlobal.get(i).size() != coreCoverage.get(i).size()){
				System.out.println("dev.error: different vector size of coverage (" + coverageGlobal.get(i).size() +
						") and core coverage (" + coreCoverage.get(i).size() + ") for contig " + features.getIds().get(i));
			}
		}
		if (devError){
			System.exit(1);
		}
		
		if (Main.DEBUGMODE){ System.out.println("-> core coverage 2"); timerStart(); }
		Coverage corecov = new Coverage(coreCoverage);
		if (corecov.calcEndCovs(readLengthsMean)){
			e.addDoubleVariable("CoreCoverageMinEndMean", corecov.getCoverageMinEndMean());
			e.addDoubleVariable("CoreCoverageMinEndSD", corecov.getCoverageMinEndSD());
			e.addDoubleVariable("CoreCoverageMinEndMedian", corecov.getCoverageMinEndMedian());
			e.addDoubleVariable("CoreCoverageMinEndMAD", corecov.getCoverageMinEndMAD());
			e.addDoubleVariable("CoreCoverageMinEndMin", corecov.getCoverageMinEndMin());
			e.addDoubleVariable("CoreCoverageMinEndMax", corecov.getCoverageMinEndMax());
			e.addDoubleVariable("CoreCoverageMaxEndMean", corecov.getCoverageMaxEndMean());
			e.addDoubleVariable("CoreCoverageMaxEndSD", corecov.getCoverageMaxEndSD());
			e.addDoubleVariable("CoreCoverageMaxEndMedian", corecov.getCoverageMaxEndMedian());
			e.addDoubleVariable("CoreCoverageMaxEndMAD", corecov.getCoverageMaxEndMAD());
			e.addDoubleVariable("CoreCoverageMaxEndMin", corecov.getCoverageMaxEndMin());
			e.addDoubleVariable("CoreCoverageMaxEndMax", corecov.getCoverageMaxEndMax());
		}
		if (Main.DEBUGMODE){ timerStop(); }

		System.gc();
		
		// calculate kmer features
		boolean calcKmers = Boolean.parseBoolean(args[7]);
		if (calcKmers){
			if (Main.DEBUGMODE){ System.out.println("-> kmer"); timerStart(); }
			int kmerSize = 8;
			int ends = (int) (Statistic.mean(Statistic.means(readLengths))*0.8);
			Kmer kmer = new Kmer(features.getConsensusSequences(), kmerSize);
			e.addIntegerVariable("KmerSize", kmer.getKmerSize());
			e.addIntegerVariable("KmerGlobalUniqueness", kmer.getKmerGlobalUniqueness());
			e.addIntegerVariable("KmerEndUniquenessMin", kmer.getKmerEndUniquenessMin(ends));
			e.addIntegerVariable("KmerEndUniquenessMax", kmer.getKmerEndUniquenessMax(ends));
			e.addDoubleVariable("KmerDistanceMin", Statistic.mins(kmer.getKmerDistances()));
			if (Main.DEBUGMODE){ timerStop(); }
		}

		
		if (Main.DEBUGMODE){ System.out.println("-> export"); }

		System.gc();
		
		// export features to table format text
		String outFilename = args[2];
		e.write(outFilename);
		
		// export Coverage and CoreCoverage to fasta like text
		System.gc();
		e.writeCoverage(args[3], features.getIds(), coverageGlobal);
		System.gc();
		e.writeCoverage(args[4], features.getIds(), coreCoverage);
	}
	
	// starts a simple timer
	private static void timerStart(){
		timer = System.currentTimeMillis();
	}
	
	// stops the simple timer
	private static void timerStop(){
		System.out.println((System.currentTimeMillis() - timer) + " ms");
	}
	
}
