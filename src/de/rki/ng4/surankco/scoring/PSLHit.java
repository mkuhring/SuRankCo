/* Copyright (c) 2014,
 * Mathias Kuhring, KuhringM@rki.de, Robert Koch Institute, Germany, 
 * All rights reserved. For details, please note the license.txt.
 */

package de.rki.ng4.surankco.scoring;

import java.util.Vector;

public class PSLHit {

	private String qName, strand;
	private int qStart, qEnd, qSize;
	
	private String tName;
	private int tStart, tEnd, tSize;
	
	private Vector<String> lines;
	
	private String query, target;
	
	private Vector<Boolean> matchSeq;
	private Vector<Boolean> matchSeqExt;
	
	
	public PSLHit(String header){
		parseHeader(header);
		lines = new Vector<String>();
	}
	
	private void parseHeader(String header){
		// >contig00001:7626+41627 of 41627 gi|170079663|ref|NC_010473.1|:563130+597131 of 4686137
		String[] mainSplits = header.split(" ");
		
		String[] namePositionSplits = mainSplits[0].split("\\:");
		String[] positionSplits;
		if (namePositionSplits[1].contains("+")){
			positionSplits = namePositionSplits[1].split("\\+");
			strand = "+";
		}
		else{
			positionSplits = namePositionSplits[1].split("\\-");
			strand = "-";
		}
		qName = namePositionSplits[0].substring(1);
		qStart = Integer.parseInt(positionSplits[0]);
		qEnd = Integer.parseInt(positionSplits[1]);
		qSize = Integer.parseInt(mainSplits[2]);
		
		namePositionSplits = mainSplits[3].split("\\:");
		positionSplits = namePositionSplits[1].split("\\+");
		tName = namePositionSplits[0];
		tStart = Integer.parseInt(positionSplits[0]);
		tEnd = Integer.parseInt(positionSplits[1]);
		tSize = Integer.parseInt(mainSplits[5]);
	}
	
	private String getHeader(){
		return ">" + qName + ":" + qStart + strand + qEnd + " of " + qSize + " " +
					tName + ":" + tStart + "+" + tEnd + " of " + tSize;
	}
	
	private String getAlignment(){
		StringBuffer buffer = new StringBuffer();
		for (String line : lines){
			buffer.append(line + "\n");
		}
		return buffer.substring(0, buffer.length()-1);
	}
	
	public String toString(){
		return getHeader() + "\n" + getAlignment();
	}

	public void appendAlignment(String line) {
		lines.add(line);
	}

	public void parseAlignment() {
		StringBuffer query = new StringBuffer();
		StringBuffer target = new StringBuffer();
		
		int index = 0;
		
		while (index+2 <= lines.size()){
			query.append(lines.get(index++));
			index++;
			target.append(lines.get(index++));
			index++;
		}
		
		this.query = query.toString().toUpperCase();
		this.target = target.toString().toUpperCase();
	}
	
	public Vector<Boolean> getMatchSequence(){
		if (matchSeq != null) return matchSeq;
		
		matchSeq = new Vector<Boolean>();
		
		for (int i=0; i<query.length(); i++){
			matchSeq.add(query.charAt(i) == target.charAt(i));
		}
		
		return matchSeq;
	}
	
	public Vector<Boolean> getExtendedMatchSequence(){
		if (matchSeqExt != null) return matchSeqExt;
		
		matchSeqExt = new Vector<Boolean>();
		
		int prefix = qStart;
		for (int j=0; j<prefix; j++){
			matchSeqExt.add(false);
		}
		
		matchSeqExt.addAll(getMatchSequence());
		
		int suffix = qSize-qEnd;
		for (int j=0; j<suffix; j++){
			matchSeqExt.add(false);
		}
		
		return matchSeqExt;
	}
	
	public String getQueryName(){
		return qName;
	}
	
	public int getQuerySize(){
		return qSize;
	}

	public int getTargetSize(){
		return tSize;
	}
}
