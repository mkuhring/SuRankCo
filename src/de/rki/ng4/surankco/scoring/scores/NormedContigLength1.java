/* Copyright (c) 2014,
 * Mathias Kuhring, KuhringM@rki.de, Robert Koch Institute, Germany, 
 * All rights reserved. For details, please note the license.txt.
 */

package de.rki.ng4.surankco.scoring.scores;

import java.util.Vector;

import de.rki.ng4.surankco.scoring.PSLHit;

public class NormedContigLength1 extends Score{

	private static final String SCORE_NAME = "NormedContigLength1";
	
	@Override
	public String getScoreName() {
		return SCORE_NAME;
	}
	
	@Override
	public double calculate(PSLHit hit) {

		int contigSize = hit.getQuerySize();
		Vector<Boolean> matchSequence = hit.getExtendedMatchSequence();
		
		return (double) contigSize / (double) matchSequence.size();
	}
}
