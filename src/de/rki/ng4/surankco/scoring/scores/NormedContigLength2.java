/* Copyright (c) 2014,
 * Mathias Kuhring, KuhringM@rki.de, Robert Koch Institute, Germany, 
 * All rights reserved. For details, please note the license.txt.
 */

package de.rki.ng4.surankco.scoring.scores;

import de.rki.ng4.surankco.scoring.PSLHit;


public class NormedContigLength2 extends Score{

	private static final String SCORE_NAME = "NormedContigLength2";
	
	@Override
	public String getScoreName() {
		return SCORE_NAME;
	}
	
	@Override
	public double calculate(PSLHit hit) {

		int contigSize = hit.getQuerySize();
		int referenceSize = hit.getTargetSize();
		
		return (double) contigSize / (double) referenceSize;
	}
}
